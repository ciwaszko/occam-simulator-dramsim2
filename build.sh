#!/bin/bash

cd package
make

# Create default traces
cd traces

pyenv local 2.7.6
python ./traceParse.py k6_aoe_02_short.trc.gz
python ./traceParse.py mase_art.trc.gz

cd ../..
