/*
$(function() {
  var element = $('.dramsim2-embed');
  element.css({
    "background-color": "red"
  });

element.text("blah");

  element.on('click', function(event) {
// Obviously this won't work because it is for Sniper not DRAMSim2:
 //   $('input[name="data\[MQ\]\[R2VuZXJhbA\]\[dG90YWxfY29yZXM\]"]').val("blah");
  });


  element.on('click', function(event) {
    alert( $( this ).text() );
  });

});
*/

/*
$(function() {
  var element = $('.dramsim2-embed2');
  element.css({
    "background-color": "blue"
  });

  element.text("click to draw a circle");

  element.on('click', function(event) {
  //Make an SVG Container
  var svgContainer = d3.select('.dramsim2-embed4').append("svg")
                                      .attr("width", 200)
                                      .attr("height", 200);
  
  //Draw the Circle
  var circle = svgContainer.append("circle")
                           .attr("cx", 30)
                           .attr("cy", 30)
                           .attr("r", 20);

  });

});
*/
/*
$(function() {
  var element = $('.dramsim2-embed3');
  element.css({
    "background-color": "green"
  });

  element.text("click to draw a rectangle");

  element.on('click', function(event) {
  //Make an SVG Container
  var svgContainer = d3.select("body").append("svg")
                                      .attr("width", 200)
                                      .attr("height", 200);

  //Draw the Rectangle
  var rectangle = svgContainer.append("rect")
                           .attr("x", 10)
                           .attr("y", 10)
                           .attr("width", 50)
			   .attr("height", 50);

  });

});
*/

/*
$(function() {
  var element = $('.dramsim2-embed4');
  element.css({
    "background-color": "purple"
  });

  element.text("click to draw a line");

  element.on('click', function(event) {
   //Make an SVG Container
   var svgContainer = d3.select("body").append("svg")
                                     .attr("width", 200)
                                     .attr("height", 200);
 
   //Draw the line
   var line = svgContainer.append("line")
                          .attr("x1", 5)
                          .attr("y1", 5)
                          .attr("x2", 50)
                          .attr("y2", 50)
			  .attr("stroke-width", 2)
                          .attr("stroke", "black");
   });

});

*/



$(function() {
  var element = $('.dramsim2-embed2');
  element.css({
    "background-color": "blue"
  });

   element.text("click to draw an additional channel");

  element.on('click', function(event) {
   

  /* load ganged dual-channel diagram */
  d3.xml("/images/occam_dramsim2_additional_channel.svg", "image/svg+xml", function(xml) { 
     var importedNode = document.importNode(xml.documentElement, true);
     d3.select(".dramsim2-embed4").node().appendChild(importedNode);

  });
   });
});


$(function() {
  /* load ganged dual-channel diagram */
  d3.xml("/images/occam_dramsim2_draft.svg", "image/svg+xml", function(xml) { 
     var importedNode = document.importNode(xml.documentElement, true);
     d3.select(".viz").node().appendChild(importedNode);


        /******************************************
         *            transaction queue           *
         ******************************************/	
	/* get transaction queue depth from form */
        $(function() {
	   var currentDepth = $("input[name='data[MQ][dHJhbnNfcXVldWVfZGVwdGg]']").val();
	   var len = currentDepth.toString().length;
	   $("#transaction_queue").attr("x",(231 - ((len) * 6))); 
	   $("#transaction_queue").text(currentDepth + " bits");
	   $("#transaction_queue").css({
	      "font-weight":"bold",
              "font-size": "20px",
	      "text-align": "center"
           });   
	   $("#return_transaction_queue").attr("x",(504 - ((len) * 6))); 
	   $("#return_transaction_queue").text(currentDepth + " bits");
	   $("#return_transaction_queue").css({
	      "font-weight":"bold",
              "font-size": "20px",
	      "text-align": "center"
           });   

	});
        
	var timeoutId = 0;
	var speed = 3;

	/* mouse down on transaction queue up arrow */
        $("#transaction_queue_up").mousedown(function() {
	   $("#transaction_queue_up2").css({
              "fill": "#b3b3b3",
	      "stroke": "#333333"
	   });  
	   increment_depth();
	/* mouse up on transaction queue up arrow */
	}).bind('mouseup mouseleave', function() { 
	   clearTimeout(timeoutId);
	   speed = 3;
	   $("#transaction_queue_up2").css({
              "fill": "#E9E9E9",
	      "stroke":"#4D4D4D"
	   });                                   
        });

	/* mouse down on transaction queue up box */
        $("#transaction_queue_up2").mousedown(function() {
	   $("#transaction_queue_up2").css({
              "fill": "#b3b3b3",
	      "stroke": "#333333"
	   });     
	   increment_depth();
	/* mouse up on transaction queue up box */
	}).bind('mouseup mouseleave', function() { 
	   clearTimeout(timeoutId);
	   speed = 3;
	   $("#transaction_queue_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
	   });                                                                                          
        });

	/* increment transaction queue depth */
	function increment_depth(){
	   var currentDepth = $("input[name='data[MQ][dHJhbnNfcXVldWVfZGVwdGg]']").val() * 2;
	   var len = currentDepth.toString().length;
           $("input[name='data[MQ][dHJhbnNfcXVldWVfZGVwdGg]']").val(currentDepth);
	   $("#transaction_queue").text(currentDepth + " bits");
	   $("#transaction_queue").attr("x",(231 - ((len)*6)));  
	   $("#return_transaction_queue").text(currentDepth + " bits");
	   $("#return_transaction_queue").attr("x",(504 - ((len)*6)));                    
           if (speed > 1){
		speed -= .8;
	   }
	   timeoutId = setTimeout(increment_depth, 200 * speed);
	}

	/* hover for transaction queue up arrow */
        $("#transaction_queue_up").hover(function() {
	    $("#transaction_queue_up").css({
              "cursor": "pointer"
           });
           $("#transaction_queue_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });	   
	},
	function() {
           $("#transaction_queue_up2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
           });	                              
        });

	/* hover for transaction queue up box */
        $("#transaction_queue_up2").hover(function() {
           $("#transaction_queue_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
	   $("#transaction_queue_up2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });    	                                  
        });
	
	/* mouse down on transaction queue down arrow */
        $("#transaction_queue_down").mousedown(function() {
           $("#transaction_queue_down2").css({
              "fill": "#b3b3b3",
	      "stroke": "#333333"
           });           
           if ($("input[name='data[MQ][dHJhbnNfcXVldWVfZGVwdGg]']").val() != 1){
	   	decrement_depth(); 
	   }                                                                      
	/* mouse up on transaction queue up arrow */
	}).bind('mouseup mouseleave', function() { 
	   clearTimeout(timeoutId);
	   speed = 3;
	   $("#transaction_queue_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
	   });      
	});

        /* mouse down on transaction queue down box */
        $("#transaction_queue_down2").mousedown(function() { 
	    $("#transaction_queue_down2").css({
              "fill": "#b3b3b3",
	      "stroke": "#333333"
           });              
	   if ($("input[name='data[MQ][dHJhbnNfcXVldWVfZGVwdGg]']").val() != 1){
                decrement_depth();                                                                           
	   }      
	/* mouse up on transaction queue up box */
	}).bind('mouseup mouseleave', function() { 
	   clearTimeout(timeoutId);
	   speed = 3;
	   $("#transaction_queue_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
	   });      
	});

	/* decrement transaction queue depth */
	function decrement_depth(){
	   var currentDepth = $("input[name='data[MQ][dHJhbnNfcXVldWVfZGVwdGg]']").val() / 2;
           var len = currentDepth.toString().length;
    	   $("input[name='data[MQ][dHJhbnNfcXVldWVfZGVwdGg]']").val(currentDepth);
	   $("#transaction_queue").text(currentDepth + " bits");
	   $("#transaction_queue").attr("x",(231 - ((len)*6)));   
	   $("#return_transaction_queue").text(currentDepth + " bits");
	   $("#return_transaction_queue").attr("x",(504 - ((len)*6))); 
           if (speed > 1){
	      speed -= .8;
	   }
	   if ($("input[name='data[MQ][dHJhbnNfcXVldWVfZGVwdGg]']").val() != 1){
	      timeoutId = setTimeout(decrement_depth, 200 * speed);
	   }
	}

	/* hover for transaction queue down arrow */
        $("#transaction_queue_down").hover(function() {
           $("#transaction_queue_down").css({
              "cursor": "pointer"
	   });
	   $("#transaction_queue_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#transaction_queue_down2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });

	/* hover for transaction queue down box */
        $("#transaction_queue_down2").hover(function() {
           $("#transaction_queue_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#transaction_queue_down2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });

        /******************************************
         *            command queue           *
         ******************************************/	
	/* get command queue depth from form */
        $(function() {
	   var currentDepth = $("input[name='data[MQ][Y21kX3F1ZXVlX2RlcHRo]']").val();
	   var len = currentDepth.toString().length;
	   $("#command_queue").attr("x",(231 - ((len) * 6))); 
	   $("#command_queue").text(currentDepth + " bits");
	   $("#command_queue").css({
	      "font-weight":"bold",
              "font-size": "20px",
	      "text-align": "center"
           });   
	});

	var timeoutId = 0;
	var speed = 3;

	/* mouse down on command queue up arrow */
        $("#command_queue_up").mousedown(function() {         
	   $("#command_queue_up2").css({
              "fill": "#b3b3b3",
		      "stroke": "#333333"
	   });
	   increment_depth2();
	/* mouse up on command queue up arrow */
	}).bind('mouseup mouseleave', function() { 
	   clearTimeout(timeoutId);
	   speed = 3;     
	   $("#command_queue_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
	   });                                
        });

	/* mouse down on command queue up box */
        $("#command_queue_up2").mousedown(function() { 
	   $("#command_queue_up2").css({
          "fill": "#b3b3b3",
	      "stroke": "#333333"
	   });
           increment_depth2();
	   /* mouse up on command queue up arrow */
	   }).bind('mouseup mouseleave', function() {                                                                              
              clearTimeout(timeoutId);
	      speed = 3;
	      $("#command_queue_up2").css({
                 "fill": "#E9E9E9",
	         "stroke": "#4D4D4D"
	      });  
	});

	/* increment command queue depth */
	function increment_depth2(){
	   var currentDepth = $("input[name='data[MQ][Y21kX3F1ZXVlX2RlcHRo]']").val() * 2;
	   var len = currentDepth.toString().length;
           $("input[name='data[MQ][Y21kX3F1ZXVlX2RlcHRo]']").val(currentDepth);
	   $("#command_queue").text(currentDepth + " bits");
	   $("#command_queue").attr("x",(231 - ((len)*6))); 

	     if (speed > 1){
	      speed -= .8;
	     }
	      timeoutId = setTimeout(increment_depth2, 200 * speed);
	}
	/* hover for command queue up arrow */
        $("#command_queue_up").hover(function() {
           $("#command_queue_up").css({
	      "cursor": "pointer"
           });
	   $("#command_queue_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#command_queue_up2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });

	/* hover for command queue up box */
        $("#command_queue_up2").hover(function() {
           $("#command_queue_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#command_queue_up2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });	

	/* mouse down on command queue down arrow */
        $("#command_queue_down").mousedown(function() {
           $("#command_queue_down2").css({
              "fill": "#b3b3b3",
		      "stroke": "#333333"
           });           
          if ($("input[name='data[MQ][Y21kX3F1ZXVlX2RlcHRo]']").val() != 1){
		decrement_depth2();                                                                                         
	   } 
	   /* mouse up on command queue down arrow */
	   }).bind('mouseup mouseleave', function() { 
	      clearTimeout(timeoutId);
	      speed = 3;             
	      $("#command_queue_down2").css({
                 "fill": "#E9E9E9",
	         "stroke": "#4D4D4D"
	      });                                                     
        });

        /* mouse down on command queue down box */
        $("#command_queue_down2").mousedown(function() {       
           $("#command_queue_down2").css({
              "fill": "#b3b3b3",
		      "stroke": "#333333"
           });        		
	       if ($("input[name='data[MQ][Y21kX3F1ZXVlX2RlcHRo]']").val() != 1){
	          decrement_depth2();                                                                           
	       }
 	    }).bind('mouseup mouseleave', function() { 
	       clearTimeout(timeoutId);
	       speed = 3;             
	       $("#command_queue_down2").css({
              "fill": "#E9E9E9",
	          "stroke": "#4D4D4D"
	       });         
	});	
	function decrement_depth2(){
	   var currentDepth = $("input[name='data[MQ][Y21kX3F1ZXVlX2RlcHRo]']").val() / 2;
           var len = currentDepth.toString().length;
    	   $("input[name='data[MQ][Y21kX3F1ZXVlX2RlcHRo]']").val(currentDepth);
	   $("#command_queue").text(currentDepth + " bits");
	   $("#command_queue").attr("x",(231 - ((len)*6)));      

           if (speed > 1){
	      speed -= .8;
	   }
	   if ($("input[name='data[MQ][Y21kX3F1ZXVlX2RlcHRo]']").val() != 1){
	   	timeoutId = setTimeout(decrement_depth2, 200 * speed);                                                                                 
	   }              
	}
	/* hover for command queue down arrow */
        $("#command_queue_down").hover(function() {
           $("#command_queue_down").css({
	      "cursor": "pointer"
           });
           $("#command_queue_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#command_queue_down2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });

	/* hover for command queue down box */
        $("#command_queue_down2").hover(function() {
           $("#command_queue_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#command_queue_down2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });

        /******************************************
         *         address mapping scheme         *
         ******************************************/
	
	/* get address mapping scheme from form */
	$(function() {
		var currentScheme = $("select[name='data[MQ][YWRkcmVzc19tYXBwaW5nX3NjaGVtZQ]'] option:selected").text();
		var scheme = currentScheme.charAt(6);	
		$("#address_mapping").text("SCHEME" + scheme);
		$("#address_mapping").css({
			"font-weight":"bold",
            "font-size": "20px",
			"text-align": "center"
        });   
	});

	var timeoutId = 0;
	var speed = 3;

	/* mouse down on address mapping scheme up arrow */
    $("#address_mapping_up").mousedown(function() {       
		$("#address_mapping_up2").css({
			"fill": "#b3b3b3",
			"stroke": "#333333"
		});                                    
		increment_scheme();
	/* mouse up on address mapping scheme up arrow */
	}).bind('mouseup mouseleave', function() { 
		clearTimeout(timeoutId);
		speed = 3;     
		$("#address_mapping_up2").css({
			"fill": "#E9E9E9",
			"stroke": "#4D4D4D"
		});      
	});

	/* mouse down on address mapping scheme up box */
    $("#address_mapping_up2").mousedown(function() {
		$("#address_mapping_up2").css({
			"fill": "#b3b3b3",
			"stroke": "#333333"
		});                                    
		increment_scheme();
	/* mouse up on address mapping scheme up arrow */
	}).bind('mouseup mouseleave', function() { 
		clearTimeout(timeoutId);
		speed = 3;     
		$("#address_mapping_up2").css({
			"fill": "#E9E9E9",
			"stroke": "#4D4D4D"
		});      
	});


	/* hover for address mapping scheme up arrow */
    $("#address_mapping_up").hover(function() {
		$("#address_mapping_up").css({
			"cursor": "pointer"
        });
        $("#address_mapping_up2").css({
			"fill": "#E9E9E9",
			"stroke": "#4D4D4D"
        });
	},
	function() {
		$("#address_mapping_up2").css({
			"fill": "#cccccc",
			"stroke": "#000000"
		});                                
    });
	
	/* hover for address mapping scheme up box */
    $("#address_mapping_up2").hover(function() {
		$("#address_mapping_up2").css({
			"fill": "#E9E9E9",
			"stroke": "#4D4D4D"
        });
	},
	function() {
		$("#address_mapping_up2").css({
			"fill": "#cccccc",
			"stroke": "#000000"
		});                                
    });
	
	function increment_scheme(){
		var currentScheme = $("select[name='data[MQ][YWRkcmVzc19tYXBwaW5nX3NjaGVtZQ]'] option:selected").text();
		var scheme = currentScheme.charAt(6);

		if (scheme == 7){
			scheme = 1;
        }
        else{
			scheme++;
        }
        $("select[name='data[MQ][YWRkcmVzc19tYXBwaW5nX3NjaGVtZQ]'] option:selected").text("scheme" + scheme);
		$("#address_mapping").text("SCHEME" + scheme);  
		
		if (speed > 1){
			speed -= .8;
	    }
	    timeoutId = setTimeout(increment_scheme, 200 * speed);
	}
	
	/* mouse down on address mapping scheme down arrow */
    $("#address_mapping_down").mousedown(function() {
		$("#address_mapping_down2").css({
			"fill": "#b3b3b3",
		    "stroke": "#333333"
        });           
        decrement_scheme();
	/* mouse up on address mapping scheme down arrow */	   
	}).bind('mouseup mouseleave', function() { 
		clearTimeout(timeoutId);
		speed = 3;     
		$("#address_mapping_down2").css({
			"fill": "#E9E9E9",
			"stroke": "#4D4D4D"
		});      
	});          
        /* mouse down on address mapping scheme down box */
        $("#address_mapping_down2").mousedown(function() {           
			$("#address_mapping_down2").css({
			"fill": "#b3b3b3",
		    "stroke": "#333333"
        });           
        decrement_scheme();
	/* mouse up on address mapping scheme down arrow */	   
	}).bind('mouseup mouseleave', function() { 
		clearTimeout(timeoutId);
		speed = 3;     
		$("#address_mapping_down2").css({
			"fill": "#E9E9E9",
			"stroke": "#4D4D4D"
		});      
	}); 

	function decrement_scheme(){
		var currentScheme = $("select[name='data[MQ][YWRkcmVzc19tYXBwaW5nX3NjaGVtZQ]'] option:selected").text();
		var scheme = currentScheme.charAt(6);

		if (scheme == 1){
			scheme = 7;
        }
        else{
			scheme--;
        }
        $("select[name='data[MQ][YWRkcmVzc19tYXBwaW5nX3NjaGVtZQ]'] option:selected").text("scheme" + scheme);
		$("#address_mapping").text("SCHEME" + scheme);
		if (speed > 1){
			speed -= .8;
	    }
	    timeoutId = setTimeout(decrement_scheme, 200 * speed);
	}
	/* hover for address mapping scheme down arrow */
        $("#address_mapping_down").hover(function() {
           $("#address_mapping_down").css({
	      "cursor": "pointer"
           });
           $("#address_mapping_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#address_mapping_down2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });

	/* hover for address mapping scheme down box */
    $("#address_mapping_down2").hover(function() {
		$("#address_mapping_down2").css({
			"fill": "#E9E9E9",
			"stroke": "#4D4D4D"
        });
	},
	function() {
		$("#address_mapping_down2").css({
			"fill": "#cccccc",
			"stroke": "#000000"
		});                                
    });
        /******************************************
         *         scheduling policy         *
         ******************************************/
	
	/* get scheduling policy from form */
        $(function() {
	   var currentPolicy = $("select[name='data[MQ][c2NoZWR1bGluZ19wb2xpY3k]'] option:selected").text();
	   currentPolicy = currentPolicy.toUpperCase();
           currentPolicy = currentPolicy.replace(/_/g," ");
	   $("#scheduling_policy").text(currentPolicy);
	   $("#scheduling_policy").attr("x",455);    
	   $("#scheduling_policy").css({
	      "color" : "#000000",
	      "font-weight":"bold",
              "font-size": "9px",
	      "text-align": "center"
           });   
	});

	var timeoutId = 0;
	var speed = 3;

	/* mouse down on scheduling policy up arrow */
    $("#scheduling_policy_up").mousedown(function() {
		$("#scheduling_policy_up2").css({
			"fill": "#b3b3b3",
			"stroke": "#333333"
		});
		change_policy()
	/* mouse up on scheduling policy up arrow */
	}).bind('mouseup mouseleave', function() { 
		clearTimeout(timeoutId);
		speed = 3;   
		$("#scheduling_policy_up2").css({
			"fill": "#E9E9E9",
			"stroke": "#4D4D4D"
	   });      
	});

	/* mouse down on scheduling policy up box */
    $("#scheduling_policy_up2").mousedown(function() {
		$("#scheduling_policy_up2").css({
			"fill": "#b3b3b3",
			"stroke": "#333333"
		});
		change_policy()
	/* mouse up on scheduling policy up box */
	}).bind('mouseup mouseleave', function() { 
		clearTimeout(timeoutId);
		speed = 3;   
		$("#scheduling_policy_up2").css({
			"fill": "#E9E9E9",
			"stroke": "#4D4D4D"
	   });      
	});
	
	function change_policy(){
		var currentPolicy = $("select[name='data[MQ][c2NoZWR1bGluZ19wb2xpY3k]'] option:selected").text();
        currentPolicy = currentPolicy.toUpperCase();
        currentPolicy = currentPolicy.replace(/_/g," ");
		$("#scheduling_policy").text(currentPolicy);
		currentPolicy = currentPolicy.toLowerCase();
        currentPolicy = currentPolicy.replace(/ /g,"_");
    	$("select[name='data[MQ][c2NoZWR1bGluZ19wb2xpY3k]'] option:selected").text(currentPolicy);                                                                                          
	}
		
	/* hover for scheduling policy up arrow */
        $("#scheduling_policy_up").hover(function() {
	   $("#scheduling_policy_up").css({
              "cursor": "pointer"
           });
           $("#scheduling_policy_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
		$("#scheduling_policy_up2").css({
			"fill": "#cccccc",
			"stroke": "#000000"
		});                                
    });
	
	/* hover for scheduling policy up box */
    $("#scheduling_policy_up2").hover(function() {
		$("#scheduling_policy_up2").css({
			"fill": "#E9E9E9",
			"stroke": "#4D4D4D"
        });
	},
	function() {
		$("#scheduling_policy_up2").css({
			"fill": "#cccccc",
			"stroke": "#000000"
		});                                
    });
	
	
	/* mouse down on scheduling policy down arrow */
        $("#scheduling_policy_down").mousedown(function() {
			$("#scheduling_policy_down2").css({
			"fill": "#b3b3b3",
			"stroke": "#333333"
		});
		change_policy()
	/* mouse up on scheduling policy up box */
	}).bind('mouseup mouseleave', function() { 
		clearTimeout(timeoutId);
		speed = 3;   
		$("#scheduling_policy_down2").css({
			"fill": "#E9E9E9",
			"stroke": "#4D4D4D"
	   });      
	});

    /* mouse down on scheduling policy down box */
    $("#scheduling_policy_down2").mousedown(function() {           
		$("#scheduling_policy_down2").css({
			"fill": "#b3b3b3",
			"stroke": "#333333"
		});
		change_policy()
	/* mouse up on scheduling policy up box */
	}).bind('mouseup mouseleave', function() { 
		clearTimeout(timeoutId);
		speed = 3;   
		$("#scheduling_policy_down2").css({
			"fill": "#E9E9E9",
			"stroke": "#4D4D4D"
		});      
	});

	/* hover for scheduling policy down arrow */
    $("#scheduling_policy_down").hover(function() {
           $("#scheduling_policy_down").css({
		"cursor": "pointer"
           });
           $("#scheduling_policy_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
		$("#scheduling_policy_down2").css({
			"fill": "#cccccc",
			"stroke": "#000000"
		});                                
    });
		
	/* hover for scheduling policy down box */
    $("#scheduling_policy_down2").hover(function() {
		$("#scheduling_policy_down2").css({
			"fill": "#E9E9E9",
			"stroke": "#4D4D4D"
        });
	},
	function() {
		$("#scheduling_policy_down2").css({
			"fill": "#cccccc",
			"stroke": "#000000"
		});                                
    });

        /******************************************
         *         queuing structure         *
         ******************************************/
	
	/* get queuing structure from form */
        $(function() {
	   var currentStructure = $("select[name='data[MQ][cXVldWluZ19zdHJ1Y3R1cmU]'] option:selected").text();
           if (currentStructure == "per_rank_per_bank"){
              $("#queuing_structure").attr("x",475);
           }
           else{
              $("#queuing_structure").attr("x",500);
	   }
	   currentStructure = currentStructure.toUpperCase();
           currentStructure = currentStructure.replace(/_/g," ");
	   $("#queuing_structure").text(currentStructure);

	       
	   $("#queuing_structure").css({
	      "color" : "#000000",
	      "font-weight":"bold",
              "font-size": "9px",
	      "text-align": "center"
           });   
	});

	var timeoutId = 0;
	var speed = 3;

	/* mouse down on queuing structure up arrow */
        $("#queuing_structure_up").mousedown(function() {
           var currentStructure = $("select[name='data[MQ][cXVldWluZ19zdHJ1Y3R1cmU]'] option:selected").text();
	   if (currentStructure == "per_rank_per_bank"){
              currentStructure = "PER RANK";
              $("#queuing_structure").attr("x",500);
           }
           else{
              currentStructure = "PER RANK PER BANK";
              $("#queuing_structure").attr("x",475);
	   }

	   $("#queuing_structure").text(currentStructure);        
	   $("#queuing_structure_up2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });
	   currentStructure = currentStructure.toLowerCase();
           currentStructure = currentStructure.replace(/ /g,"_");
           $("select[name='data[MQ][cXVldWluZ19zdHJ1Y3R1cmU]'] option:selected").text(currentStructure);                                    
        });

	/* mouse up on queuing structure up arrow */
	$("#queuing_structure_up").mouseup(function() {
	  $("#queuing_structure_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
	   });      
	});

	/* mouse down on queuing structure up box */
        $("#queuing_structure_up2").mousedown(function() {
	   var currentStructure = $("select[name='data[MQ][cXVldWluZ19zdHJ1Y3R1cmU]'] option:selected").text();
	   if (currentStructure == "per_rank_per_bank"){
              currentStructure = "PER RANK";
              $("#queuing_structure").attr("x",500);
           }
           else{
              currentStructure = "PER RANK PER BANK";
              $("#queuing_structure").attr("x",475);
	   }
	   $("#queuing_structure").text(currentStructure);
	   currentStructure = currentStructure.toLowerCase();
           currentStructure = currentStructure.replace(/ /g,"_");
    	   $("select[name='data[MQ][cXVldWluZ19zdHJ1Y3R1cmU]'] option:selected").text(currentStructure);                                                                                          
        });

	/* hover for queuing structure up arrow */
        $("#queuing_structure_up").hover(function() {
           $("#queuing_structure_up").css({
              "cursor": "pointer"
           });
	   $("#queuing_structure_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#queuing_structure_up2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });
	
	/* mouse down on queuing structure down arrow */
        $("#queuing_structure_down").mousedown(function() {
           $("#queuing_structure_down2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
           });           
	   var currentStructure = $("select[name='data[MQ][cXVldWluZ19zdHJ1Y3R1cmU]'] option:selected").text();
	   if (currentStructure == "per_rank_per_bank"){
              currentStructure = "PER RANK";
              $("#queuing_structure").attr("x",500);
           }
           else{
              currentStructure = "PER RANK PER BANK";
	      $("#queuing_structure").attr("x",475);
	   }
	   $("#queuing_structure").text(currentStructure); 
           currentStructure = currentStructure.toLowerCase();
           currentStructure = currentStructure.replace(/ /g,"_");
    	   $("select[name='data[MQ][cXVldWluZ19zdHJ1Y3R1cmU]'] option:selected").text(currentStructure);           
        });

        /* mouse up on queuing structure down arrow */
	$("#queuing_structure_down").mouseup(function() {
	   $("#queuing_structure_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
	   });      
	});

        /* mouse down on queuing structure down box */
        $("#queuing_structure_down2").mousedown(function() {           
	   var currentStructure = $("select[name='data[MQ][cXVldWluZ19zdHJ1Y3R1cmU]'] option:selected").text();
	   if (currentStructure == "per_rank_per_bank"){
              currentStructure = "PER RANK";
              $("#queuing_structure").attr("x",500);
           }
           else{
              currentStructure = "PER RANK PER BANK";
              $("#queuing_structure").attr("x",475);
	   }
	   $("#queuing_structure").text(currentStructure);
           currentStructure = currentStructure.toLowerCase();
           currentStructure = currentStructure.replace(/ /g,"_");
    	   $("select[name='data[MQ][cXVldWluZ19zdHJ1Y3R1cmU]'] option:selected").text(currentStructure);   
	});

	/* hover for queuing structure down arrow */
        $("#queuing_structure_down").hover(function() {
           $("#queuing_structure_down").css({
              "cursor": "pointer"
	   });
           $("#queuing_structure_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#queuing_structure_down2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });


        /******************************************
         *         row buffer policy              *
         ******************************************/
	
	/* get row buffer policy from form */
        $(function() {
	   var currentPolicy = $("select[name='data[MQ][cm93X2J1ZmZlcl9wb2xpY3k]'] option:selected").text();
           if (currentPolicy == "open_page"){
              $("#row_buffer").attr("x",501);
           }
           else{
              $("#row_buffer").attr("x",500);
	   }
	   currentPolicy = currentPolicy.toUpperCase();
           currentPolicy = currentPolicy.replace(/_/g," ");
	   $("#row_buffer").text(currentPolicy);

	       
	   $("#row_buffer").css({
	      "color" : "#000000",
	      "font-weight":"bold",
              "font-size": "9px",
	      "text-align": "center"
           });   
	});

	var timeoutId = 0;
	var speed = 3;

	/* mouse down on row buffer policy up arrow */
        $("#row_buffer_up").mousedown(function() {
           var currentPolicy = $("select[name='data[MQ][cm93X2J1ZmZlcl9wb2xpY3k]'] option:selected").text();
	   if (currentPolicy == "open_page"){
              currentPolicy = "CLOSE PAGE";
              $("#row_buffer").attr("x",500);
           }
           else{
              currentPolicy = "OPEN PAGE";
              $("#row_buffer").attr("x",501);
	   }
	   $("#row_buffer").text(currentPolicy);        

	   $("#row_buffer_up2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });
	   currentPolicy = currentPolicy.toLowerCase();
           currentPolicy = currentPolicy.replace(/ /g,"_");
           $("select[name='data[MQ][cm93X2J1ZmZlcl9wb2xpY3k]'] option:selected").text(currentPolicy);                                    
        });

	/* mouse up on row buffer policy up arrow */
	$("#row_buffer_up").mouseup(function() {
	  $("#row_buffer_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
	   });      
	});

	/* mouse down on row buffer policy up box */
        $("#row_buffer_up2").mousedown(function() {
	   var currentPolicy = $("select[name='data[MQ][cm93X2J1ZmZlcl9wb2xpY3k]'] option:selected").text();
	   if (currentPolicy == "open_page"){
              currentPolicy = "CLOSE PAGE";
              $("#row_buffer").attr("x",500);
           }
           else{
              currentPolicy = "OPEN PAGE";
              $("#row_buffer").attr("x",501);
	   }
	   $("#row_buffer").text(currentPolicy);
	   currentPolicy = currentPolicy.toLowerCase();
           currentPolicy = currentPolicy.replace(/ /g,"_");
    	   $("select[name='data[MQ][cm93X2J1ZmZlcl9wb2xpY3k]'] option:selected").text(currentPolicy);                                                                                          
        });

	/* hover for row buffer policy up arrow */
        $("#row_buffer_up").hover(function() {
           $("#row_buffer_up").css({
	      "cursor": "pointer"
           });
           $("#row_buffer_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#row_buffer_up2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });
	
	/* mouse down on row buffer policy down arrow */
        $("#row_buffer_down").mousedown(function() {
           $("#row_buffer_down2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
           });           
	   var currentPolicy = $("select[name='data[MQ][cm93X2J1ZmZlcl9wb2xpY3k]'] option:selected").text();
	   if (currentPolicy == "open_page"){
              currentPolicy = "CLOSE PAGE";
              $("#row_buffer").attr("x",500);
           }
           else{
              currentPolicy = "OPEN PAGE";
	      $("#row_buffer").attr("x",501);
	   }
	   $("#row_buffer").text(currentPolicy); 
           currentPolicy = currentPolicy.toLowerCase();
           currentPolicy = currentPolicy.replace(/ /g,"_");
    	   $("select[name='data[MQ][cm93X2J1ZmZlcl9wb2xpY3k]'] option:selected").text(currentPolicy);           
        });

        /* mouse up on row buffer policy down arrow */
	$("#row_buffer_down").mouseup(function() {
	   $("#row_buffer_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
	   });      
	});

        /* mouse down on row buffer policy down box */
        $("#row_buffer_down2").mousedown(function() {           
	   var currentPolicy = $("select[name='data[MQ][cm93X2J1ZmZlcl9wb2xpY3k]'] option:selected").text();
	   if (currentPolicy == "open_page"){
              currentPolicy = "CLOSE PAGE";
              $("#row_buffer").attr("x",500);
           }
           else{
              currentPolicy = "OPEN PAGE";
              $("#row_buffer").attr("x",501);
	   }
	   $("#row_buffer").text(currentPolicy);
           currentPolicy = currentPolicy.toLowerCase();
           currentPolicy = currentPolicy.replace(/ /g,"_");
    	   $("select[name='data[MQ][cm93X2J1ZmZlcl9wb2xpY3k]'] option:selected").text(currentPolicy);   
	});

	/* hover for row buffer policy down arrow */
        $("#row_buffer_down").hover(function() {
           $("#row_buffer_down").css({
              "cursor": "pointer"
           });
	   $("#row_buffer_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#row_buffer_down2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });


        /******************************************
         *            total row accesses           *
         ******************************************/	
	/* get total row accesses from form */
        $(function() {
	   var currentAccesses = parseInt($("input[name='data[MQ][dG90YWxfcm93X2FjY2Vzc2Vz]']").val());
	   var len = currentAccesses.toString().length;
	   $("#row_accesses").attr("x",(525 - ((len) * 3))); 
	   $("#row_accesses").text(currentAccesses);
	   $("#row_accesses").css({
	      "color" : "#000000",
	      "font-weight":"bold",
              "font-size": "9px",
	      "text-align": "center"
           });   
	});


	var timeoutId = 0;
	var speed = 3;
	
	/* mouse down on row accesses up arrow */
        $("#row_accesses_up").mousedown(function() {
	   var currentAccesses = parseInt($("input[name='data[MQ][dG90YWxfcm93X2FjY2Vzc2Vz]']").val()) + 1;
	   var len = currentAccesses.toString().length;
           $("input[name='data[MQ][dG90YWxfcm93X2FjY2Vzc2Vz]']").val(currentAccesses);
	   $("#row_accesses").text(currentAccesses);
	   $("#row_accesses").attr("x",(525 - ((len)*3)));          
	   $("#row_accesses_up2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                    
        });

	/* mouse up on row accesses up arrow */
	$("#row_accesses_up").mouseup(function() {
	  $("#row_accesses_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
	   });      
	});

	/* mouse down on row accesses up box */
        $("#row_accesses_up2").mousedown(function() {
	   var currentAccesses = parseInt($("input[name='data[MQ][dG90YWxfcm93X2FjY2Vzc2Vz]']").val()) + 1;
	   var len = currentAccesses.toString().length; 
           $("input[name='data[MQ][dG90YWxfcm93X2FjY2Vzc2Vz]']").val(currentAccesses);
	   $("#row_accesses").text(currentAccesses);  
	   $("#row_accesses").attr("x",(525 - ((len)*3)));                                                                                            
        });

	/* hover for row accesses up arrow */
        $("#row_accesses_up").hover(function() {
           $("#row_accesses_up").css({
              "cursor": "pointer"
           });
           $("#row_accesses_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#row_accesses_up2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });
	
	/* mouse down on row accesses down arrow */
        $("#row_accesses_down").mousedown(function() {
           $("#row_accesses_down2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
           });           
          if ($("input[name='data[MQ][dG90YWxfcm93X2FjY2Vzc2Vz]']").val() != 1){
	      var currentAccesses = parseInt($("input[name='data[MQ][dG90YWxfcm93X2FjY2Vzc2Vz]']").val()) - 1;
              var len = currentAccesses.toString().length;
    	      $("input[name='data[MQ][dG90YWxfcm93X2FjY2Vzc2Vz]']").val(currentAccesses);
	      $("#row_accesses").text(currentAccesses);
	      $("#row_accesses").attr("x",(525 - ((len)*3)));                                                                                              
	   }                                                                  
        });
        /* mouse up on row accesses down arrow */
	$("#row_accesses_down").mouseup(function() {
	   $("#row_accesses_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
	   });      
	});

        /* mouse down on row accesses down box */
        $("#row_accesses_down2").mousedown(function() {           
	   if ($("input[name='data[MQ][dG90YWxfcm93X2FjY2Vzc2Vz]']").val() != 1){
	      var currentAccesses = parseInt($("input[name='data[MQ][dG90YWxfcm93X2FjY2Vzc2Vz]']").val()) - 1;
              var len = currentAccesses.toString().length;
    	      $("input[name='data[MQ][dG90YWxfcm93X2FjY2Vzc2Vz]']").val(currentAccesses);
	      $("#row_accesses").text(currentAccesses);
	      $("#row_accesses").attr("x",(525 - ((len)*3)));                                                                                              
	   }      
	});

	/* hover for row accesses down arrow */
        $("#row_accesses_down").hover(function() {
           $("#row_accesses_down").css({
              "cursor": "pointer"
           });
           $("#row_accesses_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#row_accesses_down2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });


        /******************************************
         *            cycle count                 *
         ******************************************/	
	/* get total cycle count from form */
        $(function() {
	   var currentCycles = parseInt($("input[name='data[MQ][Y3ljbGVfY291bnQ]']").val());
	   var len = currentCycles.toString().length;
	   $("#cycle_count").attr("x",(525 - ((len) * 3))); 
	   $("#cycle_count").text(currentCycles);
	   $("#cycle_count").css({
	      "color" : "#000000",
	      "font-weight":"bold",
              "font-size": "9px",
	      "text-align": "center"
           });   
	});

	var timeoutId = 0;
	var speed = 3;

	/* mouse down on cycle count up arrow */
        $("#cycle_count_up").mousedown(function() {
	   var currentCycles = parseInt($("input[name='data[MQ][Y3ljbGVfY291bnQ]']").val()) + 1;
	   var len = currentCycles.toString().length;
           $("input[name='data[MQ][Y3ljbGVfY291bnQ]']").val(currentCycles);
	   $("#cycle_count").text(currentCycles);
	   $("#cycle_count").attr("x",(525 - ((len)*3)));          
	   $("#cycle_count_up2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                    
        });

	/* mouse up on cycle count up arrow */
	$("#cycle_count_up").mouseup(function() {
	  $("#cycle_count_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
	   });      
	});

	/* mouse down on cycle count up box */
        $("#cycle_count_up2").mousedown(function() {
	   var currentCycles = parseInt($("input[name='data[MQ][Y3ljbGVfY291bnQ]']").val()) + 1;
	   var len = currentCycles.toString().length; 
           $("input[name='data[MQ][Y3ljbGVfY291bnQ]']").val(currentCycles);
	   $("#cycle_count").text(currentCycles);  
	   $("#cycle_count").attr("x",(525 - ((len)*3)));                                                                                            
        });

	/* hover for cycle count up arrow */
        $("#cycle_count_up").hover(function() {
           $("#cycle_count_up").css({
              "cursor": "pointer"
           });
           $("#cycle_count_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#cycle_count_up2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });
	
	/* mouse down on cycle count down arrow */
        $("#cycle_count_down").mousedown(function() {
           $("#cycle_count_down2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
           });           
          if ($("input[name='data[MQ][Y3ljbGVfY291bnQ]']").val() != 1){
	      var currentCycles = parseInt($("input[name='data[MQ][Y3ljbGVfY291bnQ]']").val()) - 1;
              var len = currentCycles.toString().length;
    	      $("input[name='data[MQ][Y3ljbGVfY291bnQ]']").val(currentCycles);
	      $("#cycle_count").text(currentCycles);
	      $("#cycle_count").attr("x",(525 - ((len)*3)));                                                                                              
	   }                                                                  
        });
        /* mouse up on cycle count down arrow */
	$("#cycle_count_down").mouseup(function() {
	   $("#cycle_count_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
	   });      
	});

        /* mouse down on cycle count down box */
        $("#cycle_count_down2").mousedown(function() {           
	   if ($("input[name='data[MQ][Y3ljbGVfY291bnQ]']").val() != 1){
	      var currentCycles = parseInt($("input[name='data[MQ][Y3ljbGVfY291bnQ]']").val()) - 1;
              var len = currentCycles.toString().length;
    	      $("input[name='data[MQ][Y3ljbGVfY291bnQ]']").val(currentCycles);
	      $("#cycle_count").text(currentCycles);
	      $("#cycle_count").attr("x",(525 - ((len)*3)));                                                                                              
	   }      
	});

	/* hover for cycle count down arrow */
        $("#cycle_count_down").hover(function() {
           $("#cycle_count_down").css({
              "cursor": "pointer"
           });
           $("#cycle_count_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#cycle_count_down2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });


        /******************************************
         *            epoch length                 *
         ******************************************/	
	/* get total epoch length from form */
        $(function() {
	   var currentLength = parseInt($("input[name='data[MQ][ZXBvY2hfbGVuZ3Ro]']").val());
	   var len = currentLength.toString().length;
	   $("#epoch_length").attr("x",(525 - ((len) * 3))); 
	   $("#epoch_length").text(currentLength);
	   $("#epoch_length").css({
	      "color" : "#000000",
	      "font-weight":"bold",
              "font-size": "9px",
	      "text-align": "center"
           });   
	});

	var timeoutId = 0;
	var speed = 3;

	/* mouse down on epoch length up arrow */
        $("#epoch_length_up").mousedown(function() {
	   var currentLength = parseInt($("input[name='data[MQ][ZXBvY2hfbGVuZ3Ro]']").val()) + 1;
	   var len = currentLength.toString().length;
           $("input[name='data[MQ][ZXBvY2hfbGVuZ3Ro]']").val(currentLength);
	   $("#epoch_length").text(currentLength);
	   $("#epoch_length").attr("x",(525 - ((len)*3)));          
	   $("#epoch_length_up2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                    
        });

	/* mouse up on epoch length up arrow */
	$("#epoch_length_up").mouseup(function() {
	  $("#epoch_length_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
	   });      
	});

	/* mouse down on epoch length up box */
        $("#epoch_length_up2").mousedown(function() {
	   var currentLength = parseInt($("input[name='data[MQ][ZXBvY2hfbGVuZ3Ro]']").val()) + 1;
	   var len = currentLength.toString().length; 
           $("input[name='data[MQ][ZXBvY2hfbGVuZ3Ro]']").val(currentLength);
	   $("#epoch_length").text(currentLength);  
	   $("#epoch_length").attr("x",(525 - ((len)*3)));                                                                                            
        });

	/* hover for epoch length up arrow */
        $("#epoch_length_up").hover(function() {
           $("#epoch_length_up").css({
              "cursor": "pointer"
           });
           $("#epoch_length_up2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#epoch_length_up2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });
	
	/* mouse down on epoch length down arrow */
        $("#epoch_length_down").mousedown(function() {
           $("#epoch_length_down2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
           });           
          if ($("input[name='data[MQ][ZXBvY2hfbGVuZ3Ro]']").val() != 1){
	      var currentLength = parseInt($("input[name='data[MQ][ZXBvY2hfbGVuZ3Ro]']").val()) - 1;
              var len = currentLength.toString().length;
    	      $("input[name='data[MQ][ZXBvY2hfbGVuZ3Ro]']").val(currentLength);
	      $("#epoch_length").text(currentLength);
	      $("#epoch_length").attr("x",(525 - ((len)*3)));                                                                                              
	   }                                                                  
        });
        /* mouse up on epoch length down arrow */
	$("#epoch_length_down").mouseup(function() {
	   $("#epoch_length_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
	   });      
	});

        /* mouse down on epoch length down box */
        $("#epoch_length_down2").mousedown(function() {           
	   if ($("input[name='data[MQ][ZXBvY2hfbGVuZ3Ro]']").val() != 1){
	      var currentLength = parseInt($("input[name='data[MQ][ZXBvY2hfbGVuZ3Ro]']").val()) - 1;
              var len = currentLength.toString().length;
    	      $("input[name='data[MQ][ZXBvY2hfbGVuZ3Ro]']").val(currentLength);
	      $("#epoch_length").text(currentLength);
	      $("#epoch_length").attr("x",(525 - ((len)*3)));                                                                                              
	   }      
	});

	/* hover for epoch length down arrow */
        $("#epoch_length_down").hover(function() {
           $("#epoch_length_down").css({
              "cursor": "pointer"
           });
           $("#epoch_length_down2").css({
              "fill": "#E9E9E9",
	      "stroke": "#4D4D4D"
           });
	},
	function() {
           $("#epoch_length_down2").css({
              "fill": "#cccccc",
	      "stroke": "#000000"
	   });                                
        });


	/* ddr timing parameter graph */

    var dataMax = 100;
    function generateData(num){
        var randomArray = [];
        for(var i=0; i<num; i++)randomArray.push(Math.random()*dataMax);
        return randomArray;
    }
    var data = generateData(18);


	d3.select("#refresh_period_up2")
        .on("mousedown", function(d, i){
            data = generateData(18);
            d3.selectAll("rect.timing_bar")
                .data(data)
                .transition()
                .call(setBarHeight);
        });
    
    var barW = 40;
    var panelSVG = d3.select("#timing_chart");
    var panelX = ~~panelSVG.attr("x");
    var panelY = ~~panelSVG.attr("y");
    var panelW = ~~panelSVG.attr("width");
    var panelH = ~~panelSVG.attr("height");

        d3.selectAll("rect.timing_bar")
        .data(data)
        .enter().append("svg:rect")
        .attr("class", "timing_bar")
        .attr("width", barW)
        .attr("fill", "#5599FF")        
        .attr("stroke", "black")
        .attr("x", function(d, i){return panelX+i*panelW/data.length+10;})
        .call(setBarHeight);
    function setBarHeight(){        
        this.attr("height", function(d, i){return d/dataMax*panelH;})  
        //.attr("y", function(d, i){return panelY+panelH-(d/dataMax*panelH);})
    }
	
	/* ddr power comsumption parameter graph */
	



	





















  });
}); 