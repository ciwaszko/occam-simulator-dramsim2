import json

input_file = open('system.ini', 'w')
data = json.load(open('object.json'))["input"]["simulator"][0]["configuration"]

for k, v in data.items():
  if isinstance(v, dict):
    for k,v in v.items():
      input_file.write(k.upper()+"="+str(v)+"\n")
  else:
    input_file.write(k.upper()+"="+str(v)+"\n")
