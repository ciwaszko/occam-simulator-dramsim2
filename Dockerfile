# DOCKER-VERSION 0.11.1
FROM occam/mjqxgzi--ovrhk3tuouwweyltmuwtembrgqwtanq-
ADD . /occam/simulator-DRAMSim2
RUN apt-get update
RUN cd /occam/simulator-DRAMSim2; /bin/bash -c 'source /occam/setup_python.sh; source /occam/setup_perl.sh; cd /occam/simulator-DRAMSim2; bash /occam/simulator-DRAMSim2/build.sh'
VOLUME ["/occam/simulator-DRAMSim2"]
CMD cd /occam/simulator-DRAMSim2; /bin/bash -c 'source /occam/setup_python.sh; source /occam/setup_perl.sh; cd /job; pyenv local 2.7.6; python /occam/simulator-DRAMSim2/launch.py'
