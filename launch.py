import os
import subprocess
import json

# Gather paths
scripts_path   = os.path.dirname(__file__)
simulator_path = "%s/package" % (scripts_path)
traces_path    = "%s/traces" % (simulator_path)
binary         = "%s/DRAMSim" % (simulator_path)
job_path       = os.getcwd()

# Generate input
os.system("python %s/config.py" % (scripts_path))

# Open object.json for command line options
data = json.load(open('object.json'))["input"]["simulator"][0]["configuration"]

trace_path = "%s/%s.trc" % (traces_path, (data["trace"] or "k6_aoe_02_short"))

# Form arguments
args = [binary,
        "-t", trace_path,
        "-s", "system.ini",
        "-S", str(data["memory_size"] or 2048),
        "-d", "%s/ini/%s.ini" % (simulator_path, data["ddr_spec"] or "DDR3_micron_64M_8B_x4_sg15"),
        "-c", str(data["cycle_count"] or 10000)]

# Run
command = ' '.join(args)
print("Running %s" % (command))
print("Output in %s/output.raw" % (job_path))

output_file = open('output.raw', 'w+')
subprocess.Popen(args, stdout=output_file)
output_file.close()

# Gather results
os.system("python %s/parse.py" % (scripts_path))
